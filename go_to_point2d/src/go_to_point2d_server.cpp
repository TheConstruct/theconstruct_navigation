#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_datatypes.h>
#include <actionlib/server/simple_action_server.h>
#include <go_to_point2d/GoToPoint2DAction.h>
#include <cmath>
#include <math.h>
#include <algorithm>

class GoToPoint2DAction {
  
  public:
    GoToPoint2DAction(std::string name) : as_(nh_, name, false), action_name_(name) {
      
      // action server mandatory methods
      as_.registerGoalCallback(boost::bind(&GoToPoint2DAction::goalCB, this));
      as_.registerPreemptCallback(boost::bind(&GoToPoint2DAction::preempCB, this));
      
      // parameters
      nh_.param("min_linear_velocity", min_linear_vel_, 0.2);
      nh_.param("max_linear_velocity", max_linear_vel_, 0.8);
      nh_.param("linear_velocity_gain", linear_vel_gain_, 0.3);
      
      nh_.param("min_angular_velocity", min_angular_vel_, 0.2);
      nh_.param("max_angular_velocity", max_angular_vel_, 0.8);
      nh_.param("angular_velocity_gain", angular_vel_gain_, 0.3);
      
      nh_.param("yaw_fix_min", yaw_fix_min_, 0.1);
      nh_.param("yaw_fix_max", yaw_fix_max_, 0.5);
      nh_.param("min_distance", min_distance_, 0.1);
      
      // initiliazing variables
      initial_distance_ = -1;
      
      // topics
      sub_ = nh_.subscribe("/odom", 1, &GoToPoint2DAction::getOdometry, this);
      pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
      as_.start();
    }
    
    ~GoToPoint2DAction(void) {
    	
    }
    
    void success() {
      result_.x = x_;
      result_.y = y_;
      result_.distance = distance_;
      result_.status = status_;

      twist_.linear.x = 0;
      twist_.angular.z = 0;
      pub_.publish(twist_);

      ROS_INFO("%s: Succeeded", action_name_.c_str());
      as_.setSucceeded(result_);
    }
    
    void go_to_point() {
      // math for velocity command publishing
      float desired_yaw = atan2(goal_.y - y_, goal_.x - x_);
      status_ = 100 * (1 - (distance_ / initial_distance_));
      
      if(fix_yaw_) {
        _go_to_point_fix_yaw(desired_yaw);
      } else {
        _go_to_point_straight(desired_yaw);
      }
    }
    
    void _go_to_point_fix_yaw(float desired_yaw) {
      float ang_vel;
      
      float yaw_error, abs_yaw_error;
      yaw_error = (desired_yaw - yaw_);
      abs_yaw_error = fabs(yaw_error);
      
      if (abs_yaw_error > M_PI) {
          yaw_error = yaw_ - desired_yaw;
      }
      
      if(abs_yaw_error < yaw_fix_min_) {
        fix_yaw_ = false;
      }
      
      ang_vel = std::min(max_angular_vel_, std::max(min_angular_vel_, angular_vel_gain_ * fabs(yaw_error)));
      ang_vel = yaw_error < 0 ? -ang_vel : ang_vel;
      
      twist_.linear.x = 0;
      twist_.angular.z = ang_vel;
    }
    
    void _go_to_point_straight(float desired_yaw) {
      float lin_vel, ang_vel;
      
      float yaw_error, abs_yaw_error;
      yaw_error = (desired_yaw - yaw_);
      abs_yaw_error = fabs(yaw_error);
      
      if (abs_yaw_error > M_PI) {
          yaw_error = yaw_ - desired_yaw;
      }
      
      if(abs_yaw_error > yaw_fix_max_  && abs_yaw_error < M_PI) {
        fix_yaw_ = true;
      }
      
      lin_vel = std::max(min_linear_vel_, std::min(linear_vel_gain_ * distance_, max_linear_vel_));
      ang_vel = angular_vel_gain_ * yaw_error;
      
      twist_.linear.x = lin_vel;
      twist_.angular.z = ang_vel;
    }

    void taskToBeDone() {
      if(!as_.isActive()) return;
      
      distance_ = sqrt(pow(x_ - goal_.x, 2) + pow(y_ - goal_.y, 2));
      
      // publish result
      if(distance_ < min_distance_) {
        success();
        return;
      } else {
        go_to_point();
      }

      pub_.publish(twist_);
      
      // publish feedback
      feedback_.x = x_;
      feedback_.y = y_;
      feedback_.distance = distance_;
      feedback_.status = status_;
      // ROS_INFO("Position: (%.2f, %.2f) - Distance: %.2f", x_, y_, distance_);
      as_.publishFeedback(feedback_);
    }
    
    void goalCB() {
      goal_ = as_.acceptNewGoal()->point;
      distance_ = sqrt(pow(x_ - goal_.x, 2) + pow(y_ - goal_.y, 2));
      if(initial_distance_ == -1) {
        initial_x_ = x_;
        initial_y_ = y_;
        initial_distance_ = distance_;
      }
      fix_yaw_ = true;
      ROS_INFO("New goal accepted!");
    }
    
    void preempCB() {
      ROS_INFO("%s: Preempted", action_name_.c_str());
      as_.setPreempted();
    }
    
    void getOdometry(const nav_msgs::Odometry::ConstPtr& msg) {
      tf::Quaternion q(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
      tf::Matrix3x3 m(q);
      
      m.getRPY(roll_, pitch_, yaw_);
      angle_ = yaw_;
      
      x_ = msg->pose.pose.position.x;
      y_ = msg->pose.pose.position.y;

      this->taskToBeDone();
    }
    
  protected:
    
    // mandatory variables
    ros::NodeHandle nh_;
    actionlib::SimpleActionServer<go_to_point2d::GoToPoint2DAction> as_;
    std::string action_name_;
    go_to_point2d::GoToPoint2DFeedback feedback_;
    go_to_point2d::GoToPoint2DResult result_;
    ros::Subscriber sub_;
    ros::Publisher pub_;
    
    // initial variables
    float initial_distance_, initial_x_, initial_y_;
    // current variables
    float x_, y_, distance_, status_;
    // to use quaternion method
    double roll_, pitch_, yaw_, angle_;
    // goal variables
    geometry_msgs::Point goal_;
    // to publish variables
    geometry_msgs::Twist twist_;
    
    // parameters
    double min_linear_vel_, min_angular_vel_;
    double max_linear_vel_, max_angular_vel_;
    double linear_vel_gain_, angular_vel_gain_;
    double yaw_fix_min_, yaw_fix_max_;
    bool fix_yaw_;
    double min_distance_;
};

int main(int argc, char** argv) {
  ros::Time::init();
  ros::init(argc, argv, "go_to_point_server");
  ros::Rate loop_rate(20);
  
  GoToPoint2DAction go_to_point_server(ros::this_node::getName());
  while(ros::ok()) {
    ros::spinOnce();

    loop_rate.sleep();
  }
  
  return 0;
}
