# The Construct Navigation Packages #

This repository was created for collaborative ROS navigation packages 

### What is this repository for? ###

* Public navigation packages for general purpose

### How do I get set up? ###

* Clone the repository in your catkin workspace
* Build and launch it

### Contribution guidelines ###

* A video tutorial is welcome
* Code comments
* Keep it simple